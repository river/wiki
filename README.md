# Welcome to the river wiki!

Here you will find information, guides, tips and tricks that do not fit into the main documentation.

Feel free to contribute, just open a PR to the [codeberg repository](https://codeberg.org/river/wiki).

## Table of contents

- [How Tags Work](pages/How-tags-work.md)
- [Recommended Software](pages/Recommended-Software.md)
- [Community Layouts](pages/Community-Layouts.md)
- [River Wayland Protocols](pages/River-protocols.md)
- [Scratchpad and Sticky](pages/Scratchpad-and-Sticky.md)

## FAQ

- [How do I reload the configuration file?](#how-do-i-reload-the-configuration-file)
- [Wait, the configuration is a shell script?](#wait-the-configuration-is-a-shell-script)
- [How can I configure outputs?](#how-can-i-configure-outputs)
- [My windows are not tiled!](#my-windows-are-not-tiled)
- [Why doesn't screensharing work?](#why-doesn-t-screensharing-work)
- [How do I set a wallpaper?](#how-do-i-set-a-wallpaper)
- [River does not appear in my display managers session selector!](#river-does-not-appear-in-my-display-managers-session-selector)
- [How do I disable GTK decorations (e.g. title bar)?](#how-do-i-disable-gtk-decorations-e-g-title-bar)
- [How to autostart river at login from tty?](#how-to-autostart-river-at-login-from-tty)
- [Some windows don't have borders?](#some-windows-don-t-have-borders)
- [What are the minimum hardware requirements for running river?](#what-are-the-minimum-hardware-requirements-for-running-river)
- [Where can I get more help?](#where-can-i-get-more-help)

### How do I reload the configuration file?
You don't. The default river init is a shell script executing `riverctl` a bunch of times to configure river. If you want to change a setting, just run the corresponding `riverctl` command again with the updated value.

### Wait, the configuration is a shell script?
The default one is a shell script, yes. However, since river just tries to run an executable file called `init` on startup (for possible locations of the file see the man page), it can be anything. You could configure river using a python script or a compiled C program if you wanted to, just make sure the file is executable and in the right place.

- [Example configuration in posix shell](https://codeberg.org/river/river/src/branch/master/example/init)
- [Example configuration in zig](https://gist.github.com/Ultra-Code/4c14a5f00853203627e85bf92ba32d09)
- [Example configuration in Lua5.4](https://gist.github.com/FollieHiyuki/f598db7c548f3397e2a68e4340ac9fdc)
- [Example configuration in Perl 5.36](https://gist.github.com/wreedb/b45c0afa8b877b520025cd5764c124fd)
- [Example configuration in Nim](https://gist.github.com/yummy-licorice/4c9c79e5eae938c08279d0178645461f)
- [Example configuration in TypeScript](https://github.com/owl-from-hogvarts/river-wm-js/blob/master/src/example.ts) using [`river-wm-js`](https://www.npmjs.com/package/river-wm-js)
- [Example configuration in python using pywayland](https://gist.github.com/NickHastings/b34b4482515c8190e4cc9578df5f8928)

### How can I configure outputs?
River implements the `wlr-output-management-unstable-v1` protocol extension to allow external programs to configure its output. To keep river simple, this is the only way to configure outputs. See the [Recommended Software](pages/Recommended-Software.md) section of this wiki to find out which output configuration programs we recommend.

Should you run into issues (especially with USB-C/DisplayPort monitors) failing to change resolution or frequency try setting
`export WLR_DRM_NO_MODIFIERS=1`. See [this issue](https://gitlab.freedesktop.org/wlroots/wlroots/-/issues/3474) for details.

### My windows are not tiled!
You probably forgot to either run a layout generator or to configure the layout namespace of the output. River does not have any built-in layouts and depends entirely on external programs to dynamically arrange views. If no layout generator is active for the current output, river will fall back to floating window management. River is perfectly usable in this floating mode, thanks to the `move`, `resize` and `snap` commands.

To use a layout generator, make sure it is running (they are constantly running daemons, not one-shot programs) and then enable it in river (either per output or as a global default).

As an example, this following snippet will start rivertile, river's default and bundled layout generator, in the background and then tells river to use it on the currently focused output.
```
riverctl spawn rivertile
riverctl output-layout rivertile
```

**Behold:** The name of the layout is not guaranteed to be equal to the filename of the executable.

### Why doesn't screensharing work?

River supports the `wlr-screencopy-v1` protocol but some clients only support xdg-desktop-portal + pipewire for screensharing.

The [xdg-desktop-portal-wlr](https://github.com/emersion/xdg-desktop-portal-wlr) project provides an implementation of the required xdg-desktop-portal interfaces and communicates with river through the wlr-screencopy-v1 protocol.

Install and configure xdg-desktop-portal-wlr to make screensharing work in Firefox, Chromium, OBS, and more.

See the [xdg-desktop-portal-wlr troubleshooting checklist](https://github.com/emersion/xdg-desktop-portal-wlr/wiki/%22It-doesn't-work%22-Troubleshooting-Checklist) if you have issues.

### How do I set a wallpaper?
River itself does not support displaying image. Instead you will have to use an external program, like for example [swaybg](https://github.com/swaywm/swaybg). You can imagine these wallpaper programs to be like image viewers that just run in the background (using the layer shell).

### River does not appear in my display managers session selector!
River ships with a session file, but by default does not install it because display managers are not officially supported.

If you wish to install it, copy [`river.desktop`](https://codeberg.org/river/river/src/branch/master/contrib/river.desktop) (from the `river/contrib` directory) to `/usr/local/share/wayland-sessions/`. However, note that not every display manager supports Wayland sessions.

### River won't run on my Raspberry Pi!

Try adding `dtoverlay=vc4-kms-v3d` in `/boot/config.txt`.

### How do I disable GTK decorations (e.g. title bar)?

GTK unfortunately does not support the standard xdg-decoration protocol but rather the obsolete KDE server-decoration protocol.
See [#24](https://codeberg.org/river/river/issues/24) for more information.

#### Workaround for Firefox

In the meantime, for Firefox you can do this: Right click on toolbar and click on `"Customize Toolbar..."`. In the bottom, uncheck `"Title Bar"`.

It is also possible to remove GTK window buttons with `gsettings set org.gnome.desktop.wm.preferences button-layout ""`

#### Woraround for Emacs

```elisp
(setq default-frame-alist '((undecorated . t)))
```

#### Global workaround by modifying GTKs source
One may also apply this 1 line patch to gtk3:
```diff
diff --git a/gtk/gtkwindow.c b/gtk/gtkwindow.c
index 8df1c8e861..d102017942 100644
--- a/gtk/gtkwindow.c
+++ b/gtk/gtkwindow.c
@@ -6121,8 +6121,7 @@ gtk_window_should_use_csd (GtkWindow *window)
 #ifdef GDK_WINDOWING_WAYLAND
   if (GDK_IS_WAYLAND_DISPLAY (gtk_widget_get_display (GTK_WIDGET (window))))
     {
-      GdkDisplay *gdk_display = gtk_widget_get_display (GTK_WIDGET (window));
-      return !gdk_wayland_display_prefers_ssd (gdk_display);
+      return FALSE;
     }
 #endif
```

#### Global workaround using GTK CSS
GTK decorations can also be customized by creating or modifying the `~/.config/gtk-3.0/gtk.css` and `~/.config/gtk-4.0/gtk.css` files to hide the title bar:
```css
/* No (default) title bar on wayland */
headerbar.default-decoration {
  /* You may need to tweak these values depending on your GTK theme */
  margin-bottom: 50px;
  margin-top: -100px;
}

/* rm -rf window shadows */
window.csd,             /* gtk4? */
window.csd decoration { /* gtk3 */
  box-shadow: none;
}
```
This approach will retain the functionality of windows with customized title bars, eg. file picker dialogs with buttons in the title bar. If you want to remove those title bars as well without breaking them, change this option in `~/.config/gtk-3.0/settings.ini` and `~/.config/gtk-4.0/settings.ini` to move those buttons to the bottom of the window:
``` ini
[Settings]
gtk-dialogs-use-header=false
```

### How to autostart river at login from tty?

Add these lines to your shell's `.profile` (`.bash_profile` for Bash, `.zprofile` for ZSH):
```sh
if [ -z "$WAYLAND_DISPLAY" ] && [ $(tty) = "/dev/tty1" ]; then
  exec river
fi
```
It's going to autostart `river`, if you are on `tty1`.

If you want to redirect logs to file, add `> ~/.river.log 2>&1` after `exec river`.

### Some windows don't have borders?
You need to use Server Side Decorations(SSD), for example using `riverctl rule-add -app-id <app-id> ssd`. See the `Rules` section of `riverctl(1)` man page to learn more about how `rule-add` works.
You can get the `app-id` of an application using a client like [lswt](https://git.sr.ht/~leon_plickat/lswt).

### What are the minimum hardware requirements for running river?
River runs well on a ThinkPad X200 from 2008. Any newer hardware should be perfectly fine, as long as you don't use proprietary graphics drivers.

### Where can I get more help?
Probably the best place to get help is the [#river](https://web.libera.chat/?channels=#river) channel on irc.libera.chat.
Before actually asking, you may be able to save yourself some time by searching the [archives](https://libera.irclog.whitequark.org/river/) to see if your question has already been answered. Use the "Enter keywords" box and click "Search".
