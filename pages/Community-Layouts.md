This is a list of layout generators made by the community that can be used with river. If you know of any not yet listed here, feel free to add them!

- [riverguile](https://git.sr.ht/~leon_plickat/riverguile) - Define layouts in guile scheme
- [owm](https://github.com/justinlovinger/owm) - An experimental layout generator using mathematical optimization to invent layouts on-the-fly
- [kile](https://gitlab.com/snakedye/kile) - Define layouts using a powerful lisp-like syntax
- [stacktile](https://git.sr.ht/~leon_plickat/stacktile) - Divides the window list into sublayouts which are themselves arranged in a metalayout
- [rivercarro](https://git.sr.ht/~novakane/rivercarro) - Fork of rivertile adding monocle layout and smart gaps
- [river-luatile](https://github.com/MaxVerevkin/river-luatile) - Write custom layouts in Lua
- [river-bsp-layout](https://github.com/areif-dev/river-bsp-layout) - Binary space partitioned layout to tile windows in a grid
- [river-dwindle](https://gitlab.com/thom-cameron/river-dwindle) - A layout generator that behaves similarly to the 'spiral' layouts available in other window managers
- [filtile](https://github.com/pkulak/filtile) - Like Rivertile, but with configuration per tag, monocle layout and smart gaps.
- [wideriver](https://github.com/alex-courtis/wideriver) - Per-tag state, wide/left/right stacking, monocle with alternate borders, dwindling/diminishing/even stacks
- [river-ultitile](https://sr.ht/~midgard/river-ultitile/) - Main/stack layout with automatic centering on widescreens, optional per-output-and-tag state, build your own layouts in terms of nested tiles
- [river-spiral-extended](https://codeberg.org/ideasman42/riverwm-spiral-extended) - Binary space partitioned layout with various options to better support both vertical & horizontal monitors.