# How tags work

## Explanation

River uses tags to order and group windows on an output. Superficially tags
appear similar to workspaces of other desktops, like Sway, but they are
indeed quite different.

When you have used workspaces on other desktops, you are used to them being
separate groups of windows you switch between. Each workspace holding a
separate set of windows, with each window belonging to exactly one workspace.

This is not how tags work on river. Being strictly pedantic, tags are not
groups of windows. Being even more pedantic, there is no tag 1, tag 2, etc.,
instead tags are merely properties of outputs and windows that control which
windows are visible at any given time. Outputs on river however have distinct
sets of windows and each window belongs to exactly one output.

This property, the tag, is a bit-field of size 32. Each output and window
has one such bit-field. In simplistic terms, this bit-field contains the
information `0` or `1` ("off" or "on") a total of 32 times.

Here is a random bit-field of that size:
```
10011000000000000100000000010000
```

Because that is a bit long, this article will explain tags using bit-fields of
size 5, without loss of generality.

Now how does this control which window is visible? When compositing a scene,
river checks the tag of each window on each output and compares them.
If they both have a `1` ("on") at the exact same position in the bit-field
at least once, the window is displayed. If not, the window will not be visible.

```
Tag of Output:   01011
Tag of Window 1: 10000 <- no match, will not be displayed
Tag of Window 2: 00110 <- one match, will be displayed
Tag of Window 3: 01001 <- multiple matches, will be displayed
```

In CS terms, river performs an `AND` operation of the window tag with the
output tags and checks if the result is greater than 0.

```
for window in all_windows:
    if (output.tag & window.tag) > 0:
        window.display()
```

To control tags, river has the *set* or *toggle* operations for windows and outputs.


Setting tags is straight forward: You just provide a new bit-field which will simply
override the current one of the output or window.

The only thing of note here is how the bit-fields are encoded when you use the
`riverctl` command. Because tags are bit-fields, they can be converted to and from
decimal integers using common binary encoding. To set the tag `00001` you provide the
command with the integer `1`, to set the tag `00011` you provide the command with the
integer `3`, and so on.

It is common to mainly use tags with exactly one `1` ("on"). Luckily, the corresponding
integers are easy to generate in the shell using a bit-shift operation.

```sh
bash-$: echo $((1 << 2))
4
```

This shifts 1, which corresponds to a tag of `00001`, two times, resulting in 4,
corresponding to a tag of `00100`. This is a common pattern in the example `init`
file ~~and commonly trips up users who don't read the docs~~.

```sh
bash-$: riverctl set-focused-tags $((1 << 1))
bash-$: riverctl set-view-tags $((1 << 2))
```

Toggling is slightly more complicated. Again you provide a bit-field in the decimal
integer representation as with with the set operation. Here however river will toggle
the bit of the tag of the window or output at the exact location where the bit-field
you provided to the command has a `1` ("on").

```
Previous tag:  00100
Toggle with:   01000
Result:        01100

Toggle again:  00100
Result:        01000

Toggle again:  01011
Result:        00011
```

```sh
bash-$: riverctl toggle-focused-tags $((1 << 1))
bash-$: riverctl toggle-view-tags $((1 << 2))
```

## A Note on Words

The above uses the term "tag" to refer to the described property of each window and output.
However, colloquially when river users talk about tags, they often don't mean that property,
but rather the individual positions in the bit-field.

Imagine a window has the tag `00101`. A river user would commonly say the window "has the tags
1 and 3". Calling these positions "tag 1" and "tag 3" likely comes both from the position inside
the bit-field as well as the fact that the toggle and set operations for them are commonly bound
to the number keys.

This way of refering to tags is strictly less correct and arguably more confusing, but has
unfortunately caught on since the very beginning and now it's probably too late to change that.

## Example use case

Imagine you are working on some project. You have editor has the tag `00001` (colloquially:
"my editor is on tag 1"), a terminal where you compile the project with tag `00010` (colloquially:
"my terminal is on tag 2") and a documentation viewer with the tag `00100` (colloquially:
"my documentation is on tag 3").

While you are writing code, you are focusing tag `00001` (colloquially: "I am on tag 1").
If you quickly want to reference the documentation, you can toggle with `00100` (colloquially:
"I toggle tag 3"), which results in you now focusing the tag `00101` (colloquially: "I have
both tag 1 and 3 open"). Now both editor and the documentation viewer are visible, likely
side-by-side depending on layout. Now you want to compile. You set the focused tag to `00010`.
Now only the terminal is on your screen. You compile but notice you have an error.
You toggle toggle with `00001` and now have your editor and your terminal with your compiler
error side-by-side, allowing you to quickly find the bug. Compiling succeeds, so you toggle 
with `00010` and only your editor remains on the screen.