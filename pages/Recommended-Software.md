River is a fairly minimal compositor/window manager. As such, most people will want to augment their setup with additional "companion" software. The following is a list of such programs which are known to work well with river.

### Output configuration

- [wlopm](https://git.sr.ht/~leon_plickat/wlopm) - Query and set output power modes (like DPMS).
- [wlr-randr](https://sr.ht/~emersion/wlr-randr/) - Simple output configuration tool.
- [kanshi](https://sr.ht/~emersion/kanshi/) - Powerful output configuration daemon supporting automatically switching between different configuration profiles based on the connected outputs.
- [way-displays](https://github.com/alex-courtis/way-displays) - Auto Manage Your Wayland Displays: resolution, refresh, VRR, scale, position, hotplug/lid

### Status Bars

- [waybar](https://github.com/Alexays/Waybar) - Highly customizable status bar with a builtin `river/tags`, `river/window` (since `0.9.13`), `river/mode` (since `0.9.14`), and `river/layout` (since `0.9.18`) modules.
- [yambar](https://codeberg.org/dnkl/yambar) - Modular and lightweight status bar with support for river as of `1.5.0`.
- [levee](https://sr.ht/~andreafeletto/levee) - Statusbar for the river wayland compositor.
- [creek](https://github.com/nmeum/creek) - A lightweight dwm-like status bar.  
- [i3bar-river](https://github.com/MaxVerevkin/i3bar-river) - A port of `i3bar` for river.
- [zelbar](https://sr.ht/~novakane/zelbar/) - Compositor agnostic, status bar reading input from STDIN
- [dam](https://codeberg.org/sewn/dam) - Itsy-bitsy dwm-esque bar for river.
- [sandbar](https://github.com/kolunmi/sandbar) - dwm-like bar for river

### Program Launchers

- [fuzzel](https://codeberg.org/dnkl/fuzzel) - Application launcher similar to rofi's `drun` mode.
- [bemenu](https://github.com/Cloudef/bemenu) - Dynamic menu library and client program inspired by dmenu.
- [mew](https://codeberg.org/sewn/mew) - Dynamic menu for wayland, an effective port of dmenu to Wayland.
- [wofi](https://hg.sr.ht/~scoopta/wofi) - GTK based launcher/menu program.
- [LavaLauncher](https://git.sr.ht/~leon_plickat/lavalauncher) - A simple dock-like launcher that let's you execute shell commands by clicking on icons.
- [nwg-drawer](https://github.com/nwg-piotr/nwg-drawer) - A standalone GNOME-like application drawer.
- [tofi](https://github.com/philj56/tofi) - Tiny dynamic menu for Wayland
- [wmenu](https://codeberg.org/adnano/wmenu) - Efficient dynamic menu for Wayland (dmenu clone).

### Notification Daemons

- [mako](https://github.com/emersion/mako) - A lightweight Wayland notification daemon.
- [salut](https://gitlab.com/snakedye/salut) - An animated mouse centric notification daemon.
- [fnott](https://codeberg.org/dnkl/fnott) - Keyboard driven and lightweight Wayland notification daemon.
- [dunst](https://github.com/dunst-project/dunst) - A highly customizable X and Wayland notification daemon.
- [wired](https://github.com/Toqozz/wired-notify) - Lightweight notification daemon with highly customizable layout blocks.
- [SwayNotificationCenter](https://github.com/ErikReider/SwayNotificationCenter) - GTK based notification daemon with a control center.

### Wallpaper Tools

- [swaybg](https://github.com/swaywm/swaybg) - Simple wallpaper tool from the sway project
- [wbg](https://codeberg.org/dnkl/wbg) - Super simple wallpaper application for Wayland compositors implementing the layer-shell protocol
- [swww](https://github.com/Horus645/swww) - Animated wallpaper daemon

### Screen Lockers

- [waylock](https://codeberg.org/ifreund/waylock) -  A small screenlocker for Wayland compositors.
- [swaylock](https://github.com/swaywm/swaylock) - i3lock clone for wayland from the sway project.

### Idle Management

- [swayidle](https://github.com/swaywm/swayidle) - Idle manager from the sway project.

### Other

- [eww](https://github.com/elkowar/eww) - ElKowar's Wacky Widgets (status bars/desktop widgets)
- [lswt](https://git.sr.ht/~leon_plickat/lswt) - List Wayland toplevels in human readable and machine parseable formats.
- [river-tag-overlay](https://git.sr.ht/~leon_plickat/river-tag-overlay) - Symbolic tag indicator that pops up on tag focus change.
- [wtype](https://github.com/atx/wtype) - Virtual keyboard client which can be used as a safe way for password managers to export secrets without going through the clipboard.
- [ristate](https://gitlab.com/snakedye/ristate) - Retrieves status information from river and lists it in a json format.
- [river-bedload](https://sr.ht/~novakane/river-bedload/) - Print information about river in json format.
- [riverwm-utils](https://github.com/NickHastings/riverwm-utils) - Cycle through tags: a.k.a. next/previous tags.
- [swhkd](https://github.com/waycrate/swhkd) - sxhkd clone for Wayland (works on TTY and X11 too)
- [wayshot](https://github.com/waycrate/wayshot) - A screenshot tool for wlroots based compositors which implements zwlr-screencopy-v1. basically a grim clone written in rust
- [wob](https://github.com/francma/wob) - A lightweight overlay bar for volume, backlight, etc.
- [wf-recorder](https://github.com/ammen99/wf-recorder) - utility program for screen recording of wlroots-based compositors.
- [wl-clipboard](https://github.com/bugaevc/wl-clipboard) - Provides Wayland clipboard utilities.
- [wl-screenrec](https://github.com/russelltg/wl-screenrec) - high performance screen recorder for wlroots-based compositors.
- [wl-color-picker](https://github.com/jgmdev/wl-color-picker) A script that provides a working color picker for wayland and wlroots by leveraging [grim](https://github.com/emersion/grim) and [slurp](https://github.com/emersion/slurp).
- [river-shifttags](https://gitlab.com/akumar-xyz/river-shifttags) - Tool for shifting focused-tags left or right.
- [flow](https://github.com/stefur/flow) - Small utility with a few extra control features for river.
